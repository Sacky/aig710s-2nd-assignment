### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 547f5d7b-6512-4564-bbc6-1bda9b614221
using Pkg

# ╔═╡ 82bb3327-de1c-4a0c-a7cc-74b8e884d933
Pkg.activate("Project.toml")

# ╔═╡ 75e3968d-fc3e-4bd7-a447-d1bee1647dd4
using PlutoUI

# ╔═╡ 19a1d3cb-4af0-45c9-aa40-ba633b152f76
using Flux

# ╔═╡ 30f5d840-c14e-11eb-054b-efe060c1447b
using MLDatasets:MNIST

# ╔═╡ 5ec188f4-c5a2-4b46-930f-8acf4d7ed0b1
using Statistics

# ╔═╡ be27c2f7-7bac-473e-ab7d-bb1955ed7a6b
using Plots

# ╔═╡ 6d9163ce-6bb8-46d4-a2bb-d6884137d573
using Flux: Data.DataLoader

# ╔═╡ 79fae27b-036c-4fa1-aa43-9de9ed94c971
using Flux: onehotbatch, onecold, crossentropy, throttle, logitcrossentropy

# ╔═╡ 02fc8067-81d9-432f-9b0e-107446708c81
using Base.Iterators: repeated, partition

# ╔═╡ 1852a74f-5bb3-4f26-9377-71585e5e9498
using Flux: @epochs

# ╔═╡ 6c393168-90ba-41fa-a875-203e84f89c53
using BSON

# ╔═╡ 05f3a623-f1b8-4a64-9ed4-3d762cff593b
using Printf

# ╔═╡ c50de00a-9243-4d2c-90ac-df42b04d95e1
using Images

# ╔═╡ 2af215a3-1f7e-43d9-acfc-fb49e4d52b02
using ImageView

# ╔═╡ 518f7653-39fa-4754-b200-21d5484f0b80
using TestImages

# ╔═╡ c07280ef-fe0d-491f-bb13-0e5637030fae
using FileIO

# ╔═╡ 1f453a8f-3af9-420f-ad69-b9991da16b22
using Parameters: @with_kw

# ╔═╡ 28a837ea-87c1-4c0e-98aa-e7e6bbcbdd74
md"## Importing packages"

# ╔═╡ 9cd4183f-571b-47d1-aa5d-8705495e3a0e
img = load("C:/Users/Sacky/.julia/datadeps/chest_xray/test/NORMAL/IM-0001-0001.jpeg")

# ╔═╡ 8a811cde-f1b4-4869-a06a-a567ddbfcad2
md"## Defining some variables"

# ╔═╡ 6ea82735-6773-46a4-8e93-5c55df36c846
epochs = 10

# ╔═╡ 9849d78e-f4e7-4d38-9b39-f0fccd3c2552
batchSize = 128

# ╔═╡ 68499627-06a3-4a84-b7d1-65c82a2ca481
direct_path = "C:/Users/Sacky/.julia/datadeps/chest_xray"

# ╔═╡ 493c5ddd-d46b-4dc3-80cc-8a17c44f6f06
md"## loading MNIST train data from Flux.Data.MNIST"

# ╔═╡ dc5a5a0c-5017-4a2b-b6c6-21e7db0afd92
Xtrain, Ytrain = MNIST.traindata()

# ╔═╡ cab4491e-a9c9-4539-be92-71188f6df3d9
md"## loading MNIST test data from Flux.Data.MNIST"

# ╔═╡ aae437ca-3a3e-4caa-a036-fa3de0e27f15
Xtest, Ytest = MNIST.testdata()

# ╔═╡ c8416ea7-9978-4f84-ac22-56a75daa50ff
md"## Exploration of the  image dataset"

# ╔═╡ 9a1a19a4-4ed8-4ab9-9c73-58dc11e74b03
size(Xtrain)

# ╔═╡ 49f24066-74f2-4ac9-a677-1a7806c0bf69
size(Ytrain)

# ╔═╡ 25ff2137-87d5-49e6-af20-34d3755553bf
size(Xtest)

# ╔═╡ ff6b5f7b-8d24-4f19-a792-caf2bb13c8b9
size(Ytest)

# ╔═╡ 0cb52f34-0bf1-4681-88ba-8255ab28ce64
MNIST.convert2image(MNIST.traintensor(1))

# ╔═╡ 5afe318c-cc34-4c48-8e64-4227939a49d7
MNIST.traintensor()

# ╔═╡ 5ddedc3d-060a-4ac6-b661-7d0788f4c8ec
MNIST.trainlabels(1:3)

# ╔═╡ 462409c2-fe8d-4ad8-940a-cb923a6d2b3c
md"## Transforming images to arrays"

# ╔═╡ eda08f91-a3a3-4306-99fc-dcebfb016718
function make_batch(X, Y, idxs)
    Xbatch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        Xbatch[:, :, :, i] = Float32.(X[idxs[i]])
    end
	#encode labels in a matrix of 1's and 0's , using 0:9 as the possible class value
    Ybatch = onehotbatch(Y[idxs], 0:9)
    return (Xbatch, Ybatch)
end

# ╔═╡ f4204d71-e6da-4ce1-84bf-831c5b9e8c87
mb_idxs = partition(1:length(Ytrain), batchSize)

# ╔═╡ cc4e81b7-b64f-45a5-908e-6d4cb0ace309
md"## Defing model architecture"

# ╔═╡ 13e7c471-0db8-48aa-bc98-b5d72fccc9d1
function build_model(args; imgSize = (28,28,1), nclasses = 6)
    cnn_output_size = Int.(floor.([imgSize[1]/8,imgSize[2]/8,32]))	

    return Chain(
    # First convolution, operating upon a 28x28 image
    Conv((3, 3), imgSize[3]=>16, pad=(1,1), stride=2, relu),
    MaxPool((2,2)),
		
	# Second convolution, operating upon a 14x14 image
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    MaxPool((2,2)),

    # Third convolution, operating upon a 7x7 image
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    MaxPool((2,2)),

    # Reshape 3d tensor into a 2d one using `Flux.flatten`, at this point it should be (3, 3, 32, N)
    flatten,
    Dense(prod(cnn_output_size), 6))
end

# ╔═╡ e30d703b-a5ec-4ad7-b4b9-3773ffba5878
md"## Defining utility"

# ╔═╡ d3b255f1-e281-4030-ad74-1d51df93da29
begin
	augment(x) = x .+ gpu(0.1f0*randn(eltype(x), size(x)))
	
	# Returns a vector of all parameters used in model
    paramvec(m) = hcat(map(p->reshape(p, :), params(m))...)

    # Function to check if any element is NaN or not
    anynan(x) = any(isnan.(x))

    accuracy(x, y, model) = mean(onecold(cpu(model(x))) .== onecold(cpu(y)))
end

# ╔═╡ cbb667f0-d1b5-483c-8bdd-68825081ffe0
md"## function train"

# ╔═╡ 2c596a3a-c07d-47a7-9041-ecb5b77fc73a
function train(; kws...)	
    args = Args(; kws...)

    train_set, test_set = processed_data(args)

    model = build_model(args) 
    
    model(train_set[1][1])
end

# ╔═╡ 67ffc0d7-f67c-4a29-b855-02b18ef58cc8
#begin
	# Getting predictions
	#ŷ = model(Xtrain)
	# Decoding predictions
	#ŷ = onecold(ŷ)
	#println("Prediction of the first image: $(ŷ[1])")
#end	

# ╔═╡ b0a145d5-d4c5-44df-99b1-ef946648f1fb
md"## function loss"

# ╔═╡ 37eb3066-c8b5-4837-9503-e3d7d193accd
function loss(x, y)
	x̂ = augment(x)
    ŷ = model(x̂)
    return logitcrossentropy(ŷ, y)
end      

# ╔═╡ d1cee9bf-9edd-4526-8800-94c0c24d8d71
md"## optimiser"

# ╔═╡ 172d3d09-fbe7-4757-91b7-b1aaae101d37
# learning rate

# ╔═╡ 07f7e54e-57cc-47e5-af6a-3f28a76120c3
lr = 0.1

# ╔═╡ c19e46db-f34e-4d53-946d-9e5c56a17223
opt = Descent(lr)

# ╔═╡ 74e84477-187d-4788-b3e9-7299bb425f70
# ADAM optimiser

# ╔═╡ 6677771e-23bf-4619-82cb-aebd2262af6f
#opt = ADAM(0.1)

# ╔═╡ 98f58bed-90c9-4b5e-94bb-2bf9e72d5747
# params function

# ╔═╡ c8e3fde3-ae2f-48fa-9ed2-8b36d458fffc
#ps = Flux.params(model)

# ╔═╡ 2480e917-25ea-434b-844a-f3b4c3ecdb99
md"## model updates"

# ╔═╡ 203803c3-f7d7-4eeb-aaa9-3239b520935e
begin
	
    best_accuracy = 0.0
    last_improvement = 0
    for epoch_idx in 1:args.epochs
        # Train for a single epoch
        Flux.train!(loss, params(model), Ytrain, Xtrain, opt)
	
        # Terminate on NaN
        if anynan(paramvec(model))
            @error "NaN params"
            break
        end
	
        # Calculate accuracy:
        acc = accuracy(Xtest, Ytest..., model)
		
		@info(@sprintf("[%d]: Test accuracy: %.4f", epoch_idx, acc))
      
        if acc >= 0.999
			 @info(" -> Exiting: Target accuracy is reached of 99.9%")
           
            break
        end
		
		 # If this is the best accuracy so far, save the model out
        if acc >= best_accuracy
             
            BSON.@save joinpath(args.savepath, "mnist_conv.bson") params=cpu.(params(model)) epoch_idx acc
            best_acc = acc
            last_improvement = epoch_idx
        end
		 if epoch_idx - last_improvement >= 5 && opt.eta > 1e-6
            opt.eta /= 10.0
            @warn(" -> Haven't improved, dropping learning rate to $(opt.eta)!")
			 last_improvement = epoch_idx
	end
		if epoch_idx - last_improvement >= 6
            @warn(" -> converged.")
            break
		end
end

# ╔═╡ 80223c62-a275-4b38-b24d-1a420c95151d
# Testing the model, from saved model
function test(; kws...)
    args = Args(; kws...)
    
    # Loading the test data
    _,test_set = processed_data(args)
    
    # Re-constructing the model with random initial weights
    model = build_model(args)
    
    # Loading the saved parameters
    BSON.@load joinpath(args.savepath, "mnist_conv.bson") params
    
    # Loading parameters onto the model
    Flux.loadparams!(model, params)
    
    test_set = gpu.(test_set)
    model = gpu(model)
    @show accuracy(test_set...,model)
end

# ╔═╡ ab6a63c2-c946-4884-b802-834b13acfec4
function sigmoid(z)
1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ 1337dbb1-6616-4621-87e7-b2030be218de
function transform_features(xMatrix, u, o)
       XNorm = (xMatrix .- u) ./ o
       return X_norm
       end

# ╔═╡ Cell order:
# ╠═28a837ea-87c1-4c0e-98aa-e7e6bbcbdd74
# ╠═547f5d7b-6512-4564-bbc6-1bda9b614221
# ╠═82bb3327-de1c-4a0c-a7cc-74b8e884d933
# ╠═75e3968d-fc3e-4bd7-a447-d1bee1647dd4
# ╠═19a1d3cb-4af0-45c9-aa40-ba633b152f76
# ╠═30f5d840-c14e-11eb-054b-efe060c1447b
# ╠═5ec188f4-c5a2-4b46-930f-8acf4d7ed0b1
# ╠═be27c2f7-7bac-473e-ab7d-bb1955ed7a6b
# ╠═6d9163ce-6bb8-46d4-a2bb-d6884137d573
# ╠═79fae27b-036c-4fa1-aa43-9de9ed94c971
# ╠═02fc8067-81d9-432f-9b0e-107446708c81
# ╠═1852a74f-5bb3-4f26-9377-71585e5e9498
# ╠═6c393168-90ba-41fa-a875-203e84f89c53
# ╠═05f3a623-f1b8-4a64-9ed4-3d762cff593b
# ╠═c50de00a-9243-4d2c-90ac-df42b04d95e1
# ╠═2af215a3-1f7e-43d9-acfc-fb49e4d52b02
# ╠═518f7653-39fa-4754-b200-21d5484f0b80
# ╠═c07280ef-fe0d-491f-bb13-0e5637030fae
# ╠═1f453a8f-3af9-420f-ad69-b9991da16b22
# ╠═9cd4183f-571b-47d1-aa5d-8705495e3a0e
# ╠═8a811cde-f1b4-4869-a06a-a567ddbfcad2
# ╠═6ea82735-6773-46a4-8e93-5c55df36c846
# ╠═9849d78e-f4e7-4d38-9b39-f0fccd3c2552
# ╠═68499627-06a3-4a84-b7d1-65c82a2ca481
# ╠═493c5ddd-d46b-4dc3-80cc-8a17c44f6f06
# ╠═dc5a5a0c-5017-4a2b-b6c6-21e7db0afd92
# ╠═cab4491e-a9c9-4539-be92-71188f6df3d9
# ╠═aae437ca-3a3e-4caa-a036-fa3de0e27f15
# ╠═c8416ea7-9978-4f84-ac22-56a75daa50ff
# ╠═9a1a19a4-4ed8-4ab9-9c73-58dc11e74b03
# ╠═49f24066-74f2-4ac9-a677-1a7806c0bf69
# ╠═25ff2137-87d5-49e6-af20-34d3755553bf
# ╠═ff6b5f7b-8d24-4f19-a792-caf2bb13c8b9
# ╠═0cb52f34-0bf1-4681-88ba-8255ab28ce64
# ╠═5afe318c-cc34-4c48-8e64-4227939a49d7
# ╠═5ddedc3d-060a-4ac6-b661-7d0788f4c8ec
# ╠═462409c2-fe8d-4ad8-940a-cb923a6d2b3c
# ╠═eda08f91-a3a3-4306-99fc-dcebfb016718
# ╠═f4204d71-e6da-4ce1-84bf-831c5b9e8c87
# ╠═cc4e81b7-b64f-45a5-908e-6d4cb0ace309
# ╠═13e7c471-0db8-48aa-bc98-b5d72fccc9d1
# ╠═e30d703b-a5ec-4ad7-b4b9-3773ffba5878
# ╠═d3b255f1-e281-4030-ad74-1d51df93da29
# ╠═cbb667f0-d1b5-483c-8bdd-68825081ffe0
# ╠═2c596a3a-c07d-47a7-9041-ecb5b77fc73a
# ╠═67ffc0d7-f67c-4a29-b855-02b18ef58cc8
# ╠═b0a145d5-d4c5-44df-99b1-ef946648f1fb
# ╠═37eb3066-c8b5-4837-9503-e3d7d193accd
# ╠═d1cee9bf-9edd-4526-8800-94c0c24d8d71
# ╠═172d3d09-fbe7-4757-91b7-b1aaae101d37
# ╠═07f7e54e-57cc-47e5-af6a-3f28a76120c3
# ╠═c19e46db-f34e-4d53-946d-9e5c56a17223
# ╠═74e84477-187d-4788-b3e9-7299bb425f70
# ╠═6677771e-23bf-4619-82cb-aebd2262af6f
# ╠═98f58bed-90c9-4b5e-94bb-2bf9e72d5747
# ╠═c8e3fde3-ae2f-48fa-9ed2-8b36d458fffc
# ╠═2480e917-25ea-434b-844a-f3b4c3ecdb99
# ╠═203803c3-f7d7-4eeb-aaa9-3239b520935e
# ╠═80223c62-a275-4b38-b24d-1a420c95151d
# ╠═ab6a63c2-c946-4884-b802-834b13acfec4
# ╠═1337dbb1-6616-4621-87e7-b2030be218de
